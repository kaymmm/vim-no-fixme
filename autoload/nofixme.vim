"
" Filename: autoload/nofixme.vim
" Author: Keith Miyake
" Author: fisle
" License: MIT License
"

let s:save_cpo = &cpoptions
set cpoptions&vim

let s:tags = get(g:, 'nofixme_tags',
      \ 'TODO\|FIXME\|XXX\|' .
      \ '\\reference\|\\todo\|\\info\|\\fixme\|\\XXX\|\\unsure')


let s:total = 0

function s:listsum(c)
  let s:total = s:total + a:c
endfunction


function! nofixme#amount(...) abort
  if !&modifiable
      return ''
  endif

  let s:total = 0
  let l:tag = get(a:, 1, s:tags)
  let l:terms = split(l:tag,'|')
  call map(l:terms, { k,v -> s:listsum(count(join(getline(0, line('$'))), v)) })
  return s:total
endfunction

let &cpoptions = s:save_cpo
unlet s:save_cpo
